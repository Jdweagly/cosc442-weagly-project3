package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VendingMachineExceptionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();
//got this code from: https://www.baeldung.com/junit-assert-exception
	@Test
	public void invalidCodeExceptionTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		
	    exceptionRule.expect(VendingMachineException.class);
	    exceptionRule.expectMessage("Invalid code for vending machine item");
	    ven1.getItem("zzz");
	}
	
	@Test
	public void occupiedItemExceptionTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		
	    exceptionRule.expect(VendingMachineException.class);
	    exceptionRule.expectMessage("Slot D already occupied");
	    ven1.addItem(i1, "D");
	}
	
	@Test
	public void removeItemExceptionTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		
	    exceptionRule.expect(VendingMachineException.class);
	    exceptionRule.expectMessage("Slot A is empty -- cannot remove item");
	    ven1.removeItem("A");
	}
	
	@Test
	public void insertMoneyExceptionTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		
	    exceptionRule.expect(VendingMachineException.class);
	    exceptionRule.expectMessage("Invalid amount.  Amount must be >= 0");
	    ven1.insertMoney(-1);
	}
	
	@Test
	public void simpleExceptionTest() { //gets coverage of a constructor that is not used 
		Exception t = new VendingMachineException();
		assertEquals(t, t);
		
	}
	
	@Test
	public void itemExceptionTest() {
		
		
	    exceptionRule.expect(VendingMachineException.class);
	    exceptionRule.expectMessage("Price cannot be less than zero");
	    VendingMachineItem i1 = new VendingMachineItem("chips", -2.0); //throws exception
	}
}
