package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VendingMachineItemTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0); //creates items for tests
		VendingMachineItem i2 = new VendingMachineItem("water", 3.0); //second item
		VendingMachineItem i3 = new VendingMachineItem("cheese", 4.0); //third item
		
		assertEquals("chips", i1.getName());
		assertEquals("water", i2.getName());
		assertEquals("cheese", i3.getName());
		
		
		assertEquals("chips", i1.getName());
		assertEquals(2.0, i1.getPrice(), 0.1);
		//fail("Not yet implemented");
	}

}
