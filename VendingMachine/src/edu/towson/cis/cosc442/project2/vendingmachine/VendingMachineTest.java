package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;



import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.towson.cis.cosc442.project2.vendingmachine.*;

public class VendingMachineTest {
	 //VendingMachineItem i1, i2, i3;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/*
	You should write unit tests for the following classes and functions:
		�	VendingMachineItem
		o	VendingMachineItem Constructor. 
		o	String getName() 
		o	String getPrice() 
		�	VendingMachine
		o	void addItem(VendingMachineItem item, String code) 
		o	VendingMachineItem removeItem(String code) 
		o	void insertMoney(double amount) 
		o	double getBalance() 
		o	boolean makePurchase() 
		o	double returnChange()   
		
		*/
	
	
	@Test
	public void test() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachine ven2 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0); //creates items for tests
		VendingMachineItem i2 = new VendingMachineItem("water", 3.0); //second item
		VendingMachineItem i3 = new VendingMachineItem("cheese", 4.0); //third item
		
		ven1.addItem(i1, "A");
		ven1.addItem(i2, "B");
		ven1.addItem(i3, "C"); 
		
		assertEquals(10, ven1.add(4, 6)); //simple test
		assertEquals(i1, ven1.getItem("A"));
		assertEquals(i2, ven1.getItem("B"));
		assertEquals(i3, ven1.getItem("C"));
		
		assertEquals("chips", i1.getName()); //get name test 
		assertEquals(2.0, i1.getPrice(), 0.1);
		
		ven1.insertMoney(10.0);
		assertEquals(10.0, ven1.getBalance(), 0.1);
		assertEquals(true, ven1.makePurchase("A"));
		assertEquals(8.0, ven1.returnChange(), 0.1);
		
		
		
		
	}
	
	@Test
	public void cCodeTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "C");
		assertEquals(i1, ven1.getItem("C"));
		ven1.insertMoney(10.0);
		assertEquals(true, ven1.makePurchase("C"));
	}
	
	@Test
	public void dCodeTest() {
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		assertEquals(i1, ven1.getItem("D"));
		ven1.insertMoney(10.0);
		assertEquals(true, ven1.makePurchase("D"));
	}
	
	@Test
	public void purchaseTest() { //this gets full coverage for purchase method 
		VendingMachine ven1 = new VendingMachine();
		VendingMachineItem i1 = new VendingMachineItem("chips", 2.0);
		ven1.addItem(i1, "D");
		assertEquals(i1, ven1.getItem("D"));
		assertEquals(false, ven1.makePurchase("D"));
		assertEquals(false, ven1.makePurchase("A"));
		ven1.insertMoney(10.0);
		assertEquals(true, ven1.makePurchase("D"));
		assertEquals(false, ven1.makePurchase("A"));
	}

	@Test //this method is to get the last bits of coverage
	public void coverage(){
	    VendingMachine a = new VendingMachine() {
	    };
	    VendingMachineException b = new VendingMachineException() {};
	    VendingMachine c = new VendingMachine();
	    VendingMachineException d = new VendingMachineException();
	}

}
